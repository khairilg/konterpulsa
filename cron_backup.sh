#!/bin/sh
# Set variables
DB_NAME="fikky"
CRON_USER="fikky"
DB_PASS="zY^dYFV.eJ-G"

FULLDATE=$(date +"%Y-%d-%m %H:%M")
NOW=$(date +"%Y-%m-%d-%H-%M")
MYSQL_DUMP=`which mysqldump`
GIT=`which git`
TEMP_BACKUP="latest_backup.sql"
BACKUP_DIR=dump

# Check current Git status and update
${GIT} status
${GIT} pull origin HEAD

# Dump database
/usr/local/apps/mysql/bin/mysqldump -u "$CRON_USER" -pzY^dYFV.eJ-G $DB_NAME > $TEMP_BACKUP &
wait

# Make backup directory if not exists (format: {year}/{month}/)
if [ ! -d "$BACKUP_DIR" ]; then
  mkdir -p $BACKUP_DIR
fi

# Compress SQL dump
tar -cvzf $BACKUP_DIR/$DB_NAME-$NOW-sql.tar.gz $TEMP_BACKUP

# Remove original SQL dump
rm -f $TEMP_BACKUP

# Remove backup older than 3 days
find $BACKUP_DIR -type f -mtime +2 -exec rm {} +

# Add to Git and commit
${GIT} add -A
${GIT} commit -m "Automatic backup - $FULLDATE"
${GIT} push origin HEAD
${GIT} reset HEAD
git gc
